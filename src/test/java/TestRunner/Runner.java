package TestRunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="/Users/mayureshkelhar/Downloads/Framework/WebAutomation/src/test/resources/features",
        glue={"StepDefinition"},
        tags = "@regression",
        plugin = {"pretty", "html:target/site/cucumber-html-report", "json:target/cucumber.json", "json:target/cucumber.xml"})
public class Runner
{

}
