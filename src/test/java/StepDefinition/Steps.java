package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Steps {
    WebDriver driver;
        @Given("^Open the Firefox and launch the application$")
        public void open_the_Firefox_and_launch_the_application() throws Throwable {
            System.setProperty("webdriver.chrome.driver", "/Users/mayureshkelhar/Downloads/Framework/WebAutomation/src/driver/chromedriver");
            ChromeOptions ops = new ChromeOptions();
            ops.addArguments("--remote-allow-origins=*");
            driver= new ChromeDriver(ops);
            driver.manage().window().maximize();
            driver.get("http://demo.guru99.com/v4");
        }

        @When("^Enter the Username and Password$")
        public void enter_the_Username_and_Password() throws Throwable
        {
            driver.findElement(By.name("uid")).sendKeys("mngr495807");
            driver.findElement(By.name("password")).sendKeys("ebejusy");
        }

        @Then("^Reset the credential$")
        public void Reset_the_credential() throws Throwable
        {
            System.out.println("This step click on the Reset button.");
        }

    @Then("Click on login button")
    public void clickOnLoginButton() {
        driver.findElement(By.name("btnLogin")).click();
    }

    @Then("Close the driver")
    public void closeTheDriver() {
        driver.quit();
    }
}

